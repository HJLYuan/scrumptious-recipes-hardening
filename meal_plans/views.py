from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse_lazy
from meal_plans.models import MealPlan
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required


class MealPlansListView(LoginRequiredMixin, ListView):
    model = MealPlan
    template_name = "meal_plans/list.html"
    paginate_by = 10

    def get_queryset(self):
        print(dir(MealPlan.objects))

        return MealPlan.objects.filter(owner=self.request.user)


class MealPlansCreateView(LoginRequiredMixin, CreateView):
    model = MealPlan
    template_name = "meal_plans/create.html"
    fields = ["name", "date", "owner", "recipes"]
    success_url = reverse_lazy("mealplans_list")

    def form_valid(self, form):
        plan = form.save(commit=False)
        plan.owner = self.request.user
        plan.save()
        form.save_m2m()
        return redirect("mealplans_new", pk=plan.id)

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


class MealPlansDetailView(LoginRequiredMixin, DetailView):
    model = MealPlan
    template_name = "meal_plans/detail.html"

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

    # def get_context_data(self, **kwargs):
    #     try:
    #         context = super().get_context_data(**kwargs)
    #     except MealPlan.DoesNotExist:
    #         return redirect("mealplans_list")
    #     return context


class MealPlansUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meal_plans/edit.html"
    fields = ["name", "date", "owner", "recipes"]

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)

    def get_success_url(self) -> str:
        return reverse_lazy("mealplans_detail", args=[self.object.id])


class MealPlansDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plans/delete.html"
    success_url = reverse_lazy("mealplans_list")

    def get_queryset(self):
        return MealPlan.objects.filter(owner=self.request.user)


# Create your views here.
