from django.urls import path
from django.contrib.auth import views as auth_views

from meal_plans.views import (
    MealPlansListView,
    MealPlansCreateView,
    MealPlansDetailView,
    MealPlansUpdateView,
    MealPlansDeleteView,
)

urlpatterns = [
    path("", MealPlansListView.as_view(), name="mealplans_list"),
    path(
        "meal_plans/create/",
        MealPlansCreateView.as_view(),
        name="mealplans_new",
    ),
    path(
        "<int:pk>/",
        MealPlansDetailView.as_view(),
        name="mealplans_detail",
    ),
    path(
        "<int:pk>/edit/",
        MealPlansUpdateView.as_view(),
        name="meanplans_edit",
    ),
    path(
        "<int:pk>/delete/",
        MealPlansDeleteView.as_view(),
        name="mealplans_delete",
    ),
    path("accounts/login/", auth_views.LoginView.as_view(), name="login"),
]


# from recipes.views import (
#     RecipeCreateView,
#     RecipeDeleteView,
#     RecipeUpdateView,
#     log_rating,
#     RecipeDetailView,
#     RecipeListView,
# )


# urlpatterns = [
#     path("", RecipeListView.as_view(), name="recipes_list"),
#     path("new/", RecipeCreateView.as_view(), name="recipe_new"),
#     path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
#     path("<int:pk>/edit/", RecipeUpdateView.as_view(), name="recipe_edit"),
#     path("<int:pk>/delete/", RecipeDeleteView.as_view(), name="recipe_delete"),
#     path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
